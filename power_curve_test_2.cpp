#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

float attrition_curve(float diff) {
    float attrition = 0.014;
    return max((diff * attrition), 0.05f);
}

float power_curve(float diff) {
    float power = 0.1;
    return max(-1 * attrition_curve(diff),
               power - attrition_curve(diff) * attrition_curve(diff));
}

int main() {
    // Create x and y data for the graph
    vector<double> x;
    vector<double> y;
    vector<double> z;
    vector<double> n1, n2;

    for (float i = 0.0f; i < 100.1; i += 0.1f) {
        x.push_back(i);
        y.push_back(power_curve(i));
        z.push_back(0.0);
        n1.push_back(22.5);
        n2.push_back(i - 50.0);
    }

    // Output the data to a file
    std::ofstream dataFile("power_data.txt");
    if (dataFile.is_open()) {
        for (size_t i = 0; i < x.size(); ++i) {
            dataFile << x[i] << " " << y[i] << " " << z[i] << " " << n1[i]
                     << " " << n2[i] << "\n";
        }
        dataFile.close();
    } else {
        std::cerr << "Unable to open file." << std::endl;
        return 1;
    }

    // Plot the data using Gnuplot
    // std::string gnuplotCommand = "gnuplot -persist -e \"set xrange [0:80];
    // set yrange [-0.25:0.25]; plot 'power_data.txt' with lines title
    // 'Attrition / Diff'\"";
    std::string gnuplotCommand =
        "gnuplot -persist -e \"set xrange [0:40]; set yrange [-0.25:0.25]; set "
        "datafile separator ' '; plot 'power_data.txt' using 1:2 with lines "
        "title 'Power / Diff', 'power_data.txt' using 1:3 with line title '', "
        "'power_data.txt' using 4:5 with line title ''\"";

    system(gnuplotCommand.c_str());

    return 0;
}
