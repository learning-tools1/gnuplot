#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

using namespace std;

float power_curve(float diff) {
    float power = 0.1;
    float loss = 0.005;
    // return max((diff * attrition), 0.1f);
    return min(power - (diff*loss), 0.1f);
    // return diff < 2.0f ? 0.1 : diff * attrition;
}

int main()
{
    // Create x and y data for the graph
    // std::vector<double> x = {1, 2, 3, 4, 5};
    // std::vector<double> y = {1, 4, 9, 16, 25};
    vector<double> x;
    vector<double> y;
    vector<double> z;


    for(float i=0.0f; i<100.1; i+=0.1f) {
        x.push_back(i);
        y.push_back(power_curve(i));
        z.push_back(0.0);
    }

    // for(auto num: y) {
    //     cout << num << " ";
    // }
    // cout << endl;

    // Output the data to a file
    std::ofstream dataFile("power_data.txt");
    if (dataFile.is_open())
    {
        for (size_t i = 0; i < x.size(); ++i)
        {
            dataFile << x[i] << " " << y[i] << " " << z[i] << "\n";
        }
        dataFile.close();
    }
    else
    {
        std::cerr << "Unable to open file." << std::endl;
        return 1;
    }

    // Plot the data using Gnuplot
    // std::string gnuplotCommand = "gnuplot -persist -e \"set xrange [0:80]; set yrange [-0.15:0.15]; plot 'power_data.txt' with lines title 'Attrition / Diff'\"";
    std::string gnuplotCommand = "gnuplot -persist -e \"set xrange [0:40]; set yrange [-0.15:0.15]; set datafile separator ' '; plot 'power_data.txt' using 1:2 with lines title 'Power / Diff', 'power_data.txt' using 1:3 with line title ''\"";
    
    system(gnuplotCommand.c_str());

    return 0;
}
