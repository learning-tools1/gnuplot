#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

using namespace std;

float attrition_curve(float diff) {
    float attrition = 0.014;
    return max((diff * attrition), 0.05f);
}

int main()
{
    // Create x and y data for the graph
    // std::vector<double> x = {1, 2, 3, 4, 5};
    // std::vector<double> y = {1, 4, 9, 16, 25};
    vector<double> x;
    vector<double> y;


    for(float i=0.0f; i<100.1; i+=0.1f) {
        x.push_back(i);
        y.push_back(attrition_curve(i));
    }

    // for(auto num: y) {
    //     cout << num << " ";
    // }
    // cout << endl;

    // Output the data to a file
    std::ofstream dataFile("attrition_data.txt");
    if (dataFile.is_open())
    {
        for (size_t i = 0; i < x.size(); ++i)
        {
            dataFile << x[i] << " " << y[i] << "\n";
        }
        dataFile.close();
    }
    else
    {
        std::cerr << "Unable to open file." << std::endl;
        return 1;
    }

    // Plot the data using Gnuplot
    std::string gnuplotCommand = "gnuplot -persist -e \"set xrange [0:20]; set yrange [0:]; plot 'attrition_data.txt' with lines title 'Attrition / Diff'\"";
    system(gnuplotCommand.c_str());

    return 0;
}
